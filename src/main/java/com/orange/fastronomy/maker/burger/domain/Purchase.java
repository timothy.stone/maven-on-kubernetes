/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */
package com.orange.fastronomy.maker.burger.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.*;
import java.time.Instant;

@Schema(description = "A logged purchase")
@Entity(name = "purchases")
public class Purchase extends PanacheEntityBase {
    @Schema(description = "Purchase's id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "Purchase's date and time", implementation = String.class, format = "date-time")
    @Column
    private Instant at;

    @Schema(description = "Purchased burger with legal traceability info (ids)")
    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "burger_id"))
    @AttributeOverride(name = "name", column = @Column(name = "burger_name"))
    @AssociationOverride(name = "ingredients", joinColumns = @JoinColumn(name = "purchase_id"))
    private Burger burger;

    public Purchase() {
    }

    public Purchase(Instant at, Burger burger) {
        this.at = at;
        this.burger = burger;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getAt() {
        return at;
    }

    public void setAt(Instant at) {
        this.at = at;
    }

    public Burger getBurger() {
        return burger;
    }

    public void setBurger(Burger burger) {
        this.burger = burger;
    }
}
