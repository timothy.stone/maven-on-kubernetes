/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */
package com.orange.fastronomy.maker.burger.service;

import com.orange.fastronomy.maker.burger.domain.Burger;
import com.orange.fastronomy.maker.burger.domain.Ingredient;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class BurgerMakerService {

    private static final String BURGER_NAME = "burger";

    public Optional<Burger> deliver() {
        return Optional.of(new Burger(
                UUID.randomUUID().toString(),
                BURGER_NAME,
                Arrays.asList(
                    new Ingredient(UUID.randomUUID().toString(), "bun"),
                    new Ingredient(UUID.randomUUID().toString(), "ketchup"),
                    new Ingredient(UUID.randomUUID().toString(), "mayo"),
                    new Ingredient(UUID.randomUUID().toString(), "tomato"),
                    new Ingredient(UUID.randomUUID().toString(), "salad"),
                    new Ingredient(UUID.randomUUID().toString(), "bacon"),
                    new Ingredient(UUID.randomUUID().toString(), "cheddar"),
                    new Ingredient(UUID.randomUUID().toString(), "steak"),
                    new Ingredient(UUID.randomUUID().toString(), "cheddar")
                )
        ));
    }
}
